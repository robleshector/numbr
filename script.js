// Data
let numbers = getNumbers();
function getNumbers() {
    let list = [];
    for(let i = 0; i <= 99; i++) {
        list.push(i);
    }
    return list;
}


// Set Variables
let card;
let liked = [];


// Assign Elements
const nextBtn = document.querySelector("#next");
const likeBtn = document.querySelector("#like");
const likedBtn = document.querySelector("#liked-btn");
const cardContainer = document.querySelector(".card-container");
const likedContainer = document.querySelector(".liked");
const likedCloseBtn = document.querySelector("#liked__close");
const likedList = document.querySelector("#liked__list");
const arrow = document.querySelector("#arrow");


// Card
function Card (num) {
    this.number = num;
    self = this;

    // Create Element
    this.cardDisplay = document.createElement("div");
    this.cardDisplay.classList.add("card");
    
    let cardNumber  = document.createElement("h5")

    // Check if last card and display proper text and disable all buttons
     if (numbers.length <= 1) {
        cardNumber.classList.add("max");
        cardNumber.innerText = "Sorry. \n There are no more numbers available";
        likeBtn.disabled = true;
        nextBtn.disabled = true;
    } else {
        cardNumber.classList.add("number")
        cardNumber.innerText = num.toString();
    }

    this.cardDisplay.appendChild(cardNumber);
    cardContainer.appendChild(this.cardDisplay);

    // Prototype Functions
    Card.prototype.newCard = () => {
        let prevCard = card;
        card = new Card(numbers[numbers.indexOf(card.number)+1] || numbers[0]);

        setTimeout(() => {
            prevCard.cardDisplay.remove();
            delete prevCard;
        }, 250);
    }

    Card.prototype.like = () => {
        liked.unshift(this.number);
        this.cardDisplay.style.animation = "toRight 250ms ease-out 1 forwards";
        this.newCard();
        numbers = numbers.filter(number => number !== this.number)

        // Check for last card and disable next button
        if (numbers.length <= 1) {
            nextBtn.disabled = true;
        }

        // Add to liked list element
        const likeElement = document.createElement("li");
        likeElement.classList.add("liked__item");
        likeElement.innerText = `You liked ${this.number}`;
        likedList.prepend(likeElement);
    }

    Card.prototype.next = () => {
        this.cardDisplay.style.animation = "toLeft 250ms ease-out 1 forwards";
        this.newCard();
    }
}


// Event Listeners
// Buttons 
nextBtn.addEventListener("click", () => {
    card.next();
}, false);

likeBtn.addEventListener("click", () => {
    card.like();
}, false);

likedBtn.addEventListener("click", () => {
    likedContainer.classList.toggle("show");
    arrow.classList.toggle("up");
}, false)

// Touch
let swipeStartPoint;

cardContainer.addEventListener("touchstart", swipeStart, false);
cardContainer.addEventListener("touchmove", swipeMove, false);


function swipeStart(e) {
    swipeStartPoint = e.touches[0].clientX;
}

function swipeMove(e) {
    e.preventDefault();

    if(!swipeStartPoint) {
        return;
    }

    let swipeEndPoint = e.touches[0].clientX;

    let diff = swipeStartPoint - swipeEndPoint;

    if(swipeStartPoint - swipeEndPoint > 0 && !nextBtn.disabled) {
        card.next();
    } else if(swipeStartPoint - swipeEndPoint < 0 && !likeBtn.disabled) {
        card.like();
    }

    swipeStartPoint = null;

}



// Initialize
card = new Card(numbers[0]);